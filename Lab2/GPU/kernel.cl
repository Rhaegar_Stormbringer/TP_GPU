__kernel void MatrixVectorMul(__global int *M, __global int *V, __global int *W, uint width, uint height, __local uint *temp)
{
    // Each work-item computes one element of W
    uint y;
    uint x;
    int finalProduct = 0;
    for (y=get_global_id(0); y<height; y+=get_group_size(0))
    {
        __global int *row = M+y*width;
        int dotProduct = 0;
        for (x=0; x<width; x+=get_local_id(0))
        {
            dotProduct += row[x]*V[x];
        }
        temp[get_local_id(0)] = dotProduct;
    
        barrier(CLK_LOCAL_MEM_FENCE);

        // Thread 0 performs the final sum
        if (get_local_id(0)==0)
        {
            finalProduct = 0;
            int i;
            for (i=0; i<get_local_size(0); i++)
            {
                finalProduct += temp[i];
            }
            W[y] = finalProduct;
        }

        barrier(CLK_LOCAL_MEM_FENCE);
    }
}
