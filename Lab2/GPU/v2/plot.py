import matplotlib.pyplot as plt
from os import system
import subprocess

nbl = 32768
nbc = 2000
n_wg = 32

t_gpu = []
n_th = []

for n_threads in range(n_wg, 512, n_wg):
    cmd = ["./gpu", str(nbc), str(nbl), str(n_threads), str(n_wg)]
    res = subprocess.run(cmd, stdout=subprocess.PIPE )
    out = res.stdout.decode('utf-8')
    print(out.split('\n')[3])
    # get GPU computation time only
    t_gpu.append( float(out.split('\n')[3].split(' ')[3] ) )
    n_th.append(n_threads)

plt.scatter(n_th, t_gpu)
plt.show()
