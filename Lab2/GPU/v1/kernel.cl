__kernel void MatrixVectorMul(__global int *M, __global int *V, __global int *W, uint width, uint height)
{
    // Each work-item computes one element of W
    uint y = get_global_id(0);
    uint x;
    __global int *row = M+y*width;
    int dotProduct = 0;

    for (x=0; x<width; x++)
    {
        dotProduct += row[x]*V[x];
    }
    W[y] = dotProduct;
}
