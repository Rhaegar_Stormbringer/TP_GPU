#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <CL/opencl.h>
#include <time.h>

#define SRC_SZ_MAX (1024)

#define PROGRAM "kernel.cl"
#define KERNEL "MatrixVectorMul"

#define ASSERT_MSG(test, ...)	  \
	if (! (test)) {		    \
		fprintf (stderr, __VA_ARGS__); \
		exit(1);		       \
	}

void read_file(char *filename, char *destination, int max_size) {
	FILE *f = fopen(filename, "r");
	ASSERT_MSG(f, "Err: read_file\n");
	int n = fread(destination, 1, max_size - 1, f); // '-1' pour laisser de la place pour '\0'.
	destination[n] = '\0';
	fclose(f);
}

int main(int argc, char** argv) {

	/****************/
	/* DECLARATIONS */
	/****************/

	int err; /* Code d'erreur. */
    uint nbl=atoi(argv[1]);
    uint nbc=atoi(argv[2]);

	char source_prg[SRC_SZ_MAX];
	/* Bien que nous n'ayons qu'un seul fichier source a compiler,
	 * il est necessaire de fournir a la fonction de compilation,
	 * un tableau dont les elements pointent vers les differents
	 * fichiers a compiler. */
	char *source = &source_prg[0];

	size_t global; /* Global domain size. */
	size_t local; /* Local domain size. */

	cl_platform_id platform_id;
	cl_device_id device_id = NULL;
	cl_context context = NULL;
	cl_command_queue commands = NULL;
	cl_program program;
	cl_kernel kernel;

	/* Pour le calcul de performances. */
	clock_t ta;
	clock_t tb;
	clock_t t1;
	clock_t t2;
	float tx_diff_ab; // Temps d'execution du main.
	float tx_diff_12; // Temps d'execution du traitement, point de vue CPU.
	cl_event event;
	cl_ulong time_start;
	cl_ulong time_end;
	double total_time; // Temps d'execution du traitement, point de vue GPU.

	/* TODO */
	/* tableaux et memoires du device. */
    int *M; // matrice d'entree
    int *V; // vecteur d'entree
    int *P; // vecteur de sortie

    /* objets OpenCl associes */
    cl_mem Mmobj = NULL;
    cl_mem Vmobj = NULL;
    cl_mem Pmobj = NULL;
	/***************/
	/* TRAITEMENTS */
	/***************/


	/* Pour le calcul de performances. */
	ta = clock();

	/* TODO */
	/* Preparation des donnees du probleme. */
    M = (int *)malloc(nbl*nbc*sizeof(int));
    V = (int *)malloc(nbc*sizeof(int));
    P = (int *)malloc(nbl*sizeof(int));

    M=malloc(nbl*nbc*sizeof(int));
    for (int i=0;i<nbl;i++) {
        for (int j=0;j<nbc;j++) {
            M[i*nbc+j]=(i+j)%12;
        }
    }

    for (int i=0;i<nbc;i++) {
        V[i]=i%10;
    }

    for (int i=0;i<nbl;i++) {
        P[i] = 0;
    }

	/* Connexion au "compute device".
	 * Possibilite de tester CL_DEVICE_TYPE_GPU ou CL_DEVICE_TYPE_CPU. */
	err = clGetPlatformIDs(1, &platform_id, NULL);
	ASSERT_MSG(err == CL_SUCCESS, "Err: clGetPlatformIDs\n");
	err = clGetDeviceIDs(platform_id, CL_DEVICE_TYPE_GPU, 1, &device_id, NULL);
	ASSERT_MSG(err == CL_SUCCESS, "Err: clGetDeviceIDs\n");

	/* Creation d'un contexte de calcul, responsable de la gestion des
	 * command_queue, de la memoire, des programmes et kernels et de
	 * l'execution. */
	context = clCreateContext(0, 1, &device_id, NULL, NULL, &err);
	ASSERT_MSG(context, "Err: clCreateContext\n");

	/* Creation d'une command_queue.
	 * XXX: pour nvidia (sgx1/2), clCreateCommandQueueWithProperties engendre un segFault. */
	commands = clCreateCommandQueue(context, device_id, CL_QUEUE_PROFILING_ENABLE, &err);
	//commands = clCreateCommandQueueWithProperties(context, device_id, props, &err);
	ASSERT_MSG(commands, "Err: clCreateCommandQueueWithProperties\n");

	/* Creation du programme. */
	read_file(PROGRAM, source, SRC_SZ_MAX);
	program = clCreateProgramWithSource(
			context,
			1,							/* Nombre de sources (fichiers) a compiler (un seul !). */
			(const char **) &source,	/* Liste des sources (fichiers). */
			NULL,						/* Nous pourrions donner les tailles... */
			&err);
	ASSERT_MSG(program, "Err: clCreateProgramWithSource\n");

	/* Compilation du programme pour le device cible.
	 * En cas d'erreur, les informations de compilation sont affiches. */
	err = clBuildProgram(program, 1, &device_id, NULL, NULL, NULL);
	if (err != CL_SUCCESS) {
		size_t log_size;
		clGetProgramBuildInfo(program, device_id, CL_PROGRAM_BUILD_LOG, 0, NULL, &log_size);
		char *log = malloc(log_size + 1);
		clGetProgramBuildInfo(program, device_id, CL_PROGRAM_BUILD_LOG, log_size + 1, log, NULL);
		log[log_size] = '\0';
		fprintf(stderr, "Err: clBuildProgram:\n%s\n", log);
		free(log);
		exit(1);
	}

	/* Creation/recuperation du kernel dans le programme. */
	kernel = clCreateKernel(program, KERNEL, &err);
	ASSERT_MSG(kernel && err == CL_SUCCESS, "Err: clCreateKernel\n");

	/* TODO */
	/* Creation des memoires dans le device. */
    Mmobj = clCreateBuffer(context, CL_MEM_READ_WRITE, nbl*nbc*sizeof(int), NULL, &err);
    Vmobj = clCreateBuffer(context, CL_MEM_READ_WRITE, nbc*sizeof(int), NULL, &err);
    Pmobj = clCreateBuffer(context, CL_MEM_READ_WRITE, nbl*sizeof(int), NULL, &err);

	/* TODO */
	/* Entree des arguments du kernel. */
    err = clSetKernelArg(kernel, 0, sizeof(cl_mem), (void *)&Mmobj);
    err = clSetKernelArg(kernel, 1, sizeof(cl_mem), (void *)&Vmobj);
    err = clSetKernelArg(kernel, 2, sizeof(cl_mem), (void *)&Pmobj);
    err = clSetKernelArg(kernel, 3, sizeof(uint), (void *)&nbc);
    err = clSetKernelArg(kernel, 4, sizeof(uint), (void *)&nbl);

    err = clEnqueueWriteBuffer(commands, Mmobj, CL_TRUE, 0, nbl*nbc*sizeof(int), M, 0, NULL, NULL);
    err = clEnqueueWriteBuffer(commands, Vmobj, CL_TRUE, 0, nbc*sizeof(int), V, 0, NULL, NULL);

	/* Recuperation de la taille maximale d'un "work group". */
	err = clGetKernelWorkGroupInfo(
			kernel, device_id, CL_KERNEL_WORK_GROUP_SIZE, sizeof(local), &local, NULL);
	ASSERT_MSG(err == CL_SUCCESS, "Err: clGetKernelWorkGroupInfo\n");

	/* Pour le calcul de performances. */
	t1 = clock();

	/* TODO */
	/* Dimensionnement du probleme. */
	global = nbl /* pas le choix vu le code du kernel */;
	local = 32 /* 32 est le nombre recommande par le systeme */;

    //printf("%d\n", global);
    //printf("%d\n", local);

	err = clEnqueueNDRangeKernel(commands, kernel, 1, NULL, &global, &local, 0, NULL, &event);
	ASSERT_MSG(err == 0, "Err: clEnqueueNDRangeKernel\n");

	/* Attente de la fin de l'execution avant de recuperer les resultats. */
	clFinish(commands); // Attendre la fin des kernel de la file.
	// Attendre la fin d'un kernel particulier (ici, focement le premier).
	//clWaitForEvents(1, &event);

	/* TODO */
	/* Lecture des resultats. */
    err = clEnqueueReadBuffer(commands, Pmobj, CL_TRUE, 0, nbl*sizeof(int), P, 0, NULL, NULL);

	/* Pour le calcul de performances. */
	t2 = clock();
	clGetEventProfilingInfo(event, CL_PROFILING_COMMAND_START, sizeof(time_start), &time_start, NULL);
	clGetEventProfilingInfo(event, CL_PROFILING_COMMAND_END, sizeof(time_end), &time_end, NULL);

	/* TODO */
	/* Affichage des resultats. */
    for (int i=0;i<nbl;i++)
        printf("%7d ",P[i]);
    printf("\n");

	/* TODO */
	/* Liberation des ressources. */
	clReleaseProgram(program);
	clReleaseKernel(kernel);
	clReleaseCommandQueue(commands);
	clReleaseContext(context);

	/* Pour le calcul de performances. */
	tb = clock();
	tx_diff_12 = ((float) (t2 - t1) / 1000000.0f ) * 1000;
	tx_diff_ab = ((float) (tb - ta) / 1000000.0f ) * 1000;
	total_time = time_end-time_start;
	printf("Temps total : %f\nTemps transfert : %f\nTemps GPU : %f\n", tx_diff_ab, tx_diff_12, total_time / 1000000.0); // En millisecondes.

	return 0;
}
