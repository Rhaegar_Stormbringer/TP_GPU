#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/times.h>
#include <math.h>
#include <time.h>

typedef unsigned int uint;

void MatrixVectorMul(const int * M, uint width, uint height,
                     const int * V, int * W)
{ 
  uint x;
  uint y;

   for (y = 0; y < height; y++) { 
     const int * row = M + y * width; 
     int dotProduct = 0; 
     for (x = 0; x < width; x++) {
       dotProduct += row[x] * V[x]; 
     }
     W[y] = dotProduct; 
   } 
} 

int main(int argc, char * argv[])
{
  int nbl=atoi(argv[1]);
  int nbc=atoi(argv[2]);
  int * M;
  int V[nbc];
  int P[nbl];
  int i,j;
  clock_t deb,fin;
  float diff;

  deb=clock();

  M=malloc(nbl*nbc*sizeof(int));
  for (i=0;i<nbl;i++) {
    for (j=0;j<nbc;j++) {
      M[i*nbc+j]=(i+j)%12;
    }
  }
  for (i=0;i<nbc;i++) {
    V[i]=i%10;
  }

  MatrixVectorMul(M,nbc,nbl,V,P); 

  for (i=0;i<nbl;i++)
    printf("%7d ",P[i]);
  printf("\n");

  fin=clock();
  diff = ((float) (fin - deb) / 1000000.0f ) * 1000;
  printf("temps total %f\n",diff);

  return(0);
}
