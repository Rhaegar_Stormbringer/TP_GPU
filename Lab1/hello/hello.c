#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <CL/opencl.h>
#include <time.h>

#define SRC_SZ_MAX (1024)
#define DATA_SZ_MAX (1024)

#define ASSERT_MSG(test, ...)          \
	if (! (test)) {                    \
		fprintf (stderr, __VA_ARGS__); \
		exit(1);                       \
	}

void read_file(char *filename, char *destination, int max_size) {
	FILE *f = fopen(filename, "r");
	ASSERT_MSG(f, "Err: read_file\n");
	size_t n = fread(destination, 1, max_size, f);
    destination[n] = 0;
	fclose(f);
}

int main(int argc, char** argv) {

	/****************/
	/* DECLARATIONS */
	/****************/

	int err; /* Code d'erreur. */

	char source_hello[SRC_SZ_MAX];
	/* Bien que nous n'ayons qu'un seul fichier source a compiler,
	 * il est necessaire de fournir a la fonction de compilation,
	 * un tableau dont les elements pointent vers les differents
	 * fichiers a compiler. */
	char *source = &source_hello[0];

	size_t global; /* Global domain size. */
	size_t local; /* Local domain size. */

	cl_platform_id platform_id;
	cl_device_id device_id = NULL;
	cl_context context = NULL;
	cl_command_queue commands = NULL;
	cl_program program;
	cl_kernel kernel;

	char data[DATA_SZ_MAX];

	/* Memoire, dans le device, pour les resultats.
	 * Le contenu de cette memoire sera rapatrie dans data. */
	cl_mem output;

	/* Pour le calcul de performances. */
	clock_t t1;
	clock_t t2;
	float tx_diff;
	cl_event event;
	cl_ulong time_start;
	cl_ulong time_end;
	double total_time;

	/***************/
	/* TRAITEMENTS */
	/***************/

	read_file("hello.cl", source, SRC_SZ_MAX);

	/* Connexion au "compute device".
	 * Possibilite de tester CL_DEVICE_TYPE_GPU ou CL_DEVICE_TYPE_CPU. */
	err = clGetPlatformIDs(1, &platform_id, NULL);
	ASSERT_MSG(err == CL_SUCCESS, "Err: clGetPlatformIDs\n");
	err = clGetDeviceIDs(platform_id, CL_DEVICE_TYPE_GPU, 1, &device_id, NULL);
	ASSERT_MSG(err == CL_SUCCESS, "Err: clGetDeviceIDs\n");

	/* Creation d'un contexte de calcul, responsable de la gestion des
	 * command_queue, de la memoire, des programmes et kernels et de
	 * l'execution. */
	context = clCreateContext(0, 1, &device_id, NULL, NULL, &err);
	ASSERT_MSG(context, "Err: clCreateContext\n");

	/* Creation d'une command_queue.
	 * XXX: pour nvidia (sgx1/2), clCreateCommandQueueWithProperties engendre un segFault. */
	commands = clCreateCommandQueue(context, device_id, CL_QUEUE_PROFILING_ENABLE, &err);
	//commands = clCreateCommandQueueWithProperties(context, device_id, props, &err);
	ASSERT_MSG(commands, "Err: clCreateCommandQueueWithProperties\n");

	/* Creation du programme. */
	program = clCreateProgramWithSource(
			context,
			1,							/* Nombre de sources (fichiers) a compiler (un seul !). */
			(const char **) &source,	/* Liste des sources (fichiers). */
			NULL,						/* Nous pourrions donner les tailles... */
			&err);
	ASSERT_MSG(program, "Err: clCreateProgramWithSource\n");

	/* Compilation du programme pour le device cible.
	 * En cas d'erreur, les informations de compilation sont affiches. */
	err = clBuildProgram(program, 1, &device_id, NULL, NULL, NULL);
	if (err != CL_SUCCESS) {
		size_t log_size;
		clGetProgramBuildInfo(program, device_id, CL_PROGRAM_BUILD_LOG, 0, NULL, &log_size);
		char *log = malloc(log_size + 1);
		clGetProgramBuildInfo(program, device_id, CL_PROGRAM_BUILD_LOG, log_size + 1, log, NULL);
		log[log_size] = '\0';
		printf("%s\n", log);
		free(log);
		exit(1);
	}

	/* Creation/recuperation du kernel dans le programme. */
	kernel = clCreateKernel(program, "hello", &err);
	ASSERT_MSG(kernel && err == CL_SUCCESS, "Err: clCreateKernel\n");

	/* Creation des memoires dans le device. */
	output = clCreateBuffer(context,
		/* Dans hello_world, cette memoire ne sera qu'ecrite par le kernel. */
		CL_MEM_WRITE_ONLY,
		sizeof(char) * DATA_SZ_MAX, NULL, NULL);
	ASSERT_MSG(output, "Err: clCreateBuffer\n");

	/* Entree des arguments du kernel. */
	err = clSetKernelArg(kernel, 0, sizeof(cl_mem), &output);
	ASSERT_MSG(err == CL_SUCCESS, "Err: clSetKernelArg\n");

	/* Recuperation de la taille maximale d'un "work group". */
	err = clGetKernelWorkGroupInfo(
			kernel, device_id, CL_KERNEL_WORK_GROUP_SIZE, sizeof(local), &local, NULL);
	ASSERT_MSG(err == CL_SUCCESS, "Err: clGetKernelWorkGroupInfo\n");

	/* Pour le calcul de performances. */
	t1 = clock();

	/* Execution en mode 1D sur les donnees en utilisant la taille maximale
	 * des "work group". */
	global = DATA_SZ_MAX;
	err = clEnqueueNDRangeKernel(
			commands, kernel, 1, NULL, &global, &local, 0, NULL, &event);
	ASSERT_MSG(err == 0, "Err: clEnqueueNDRangeKernel\n");

	/* Attente de la fin de l'execution avant de recuperer les resultats. */
	clFinish(commands); // Attendre la fin des kernel de la file.
	// Attendre la fin d'un kernel particulier (ici, focement le premier).
	//clWaitForEvents(1, &event);

	/* Lecture des resultats. */
	err = clEnqueueReadBuffer(
		commands, output, CL_TRUE, 0, sizeof(char) * SRC_SZ_MAX, data, 0, NULL, NULL); 
	ASSERT_MSG(err == CL_SUCCESS, "Err: clEnqueueReadBuffer\n");

	/* Pour le calcul de performances. */
	t2 = clock();
	tx_diff = ((float) (t2 - t1) / 1000000.0f ) * 1000;
	printf("time (CPU point of view): %f\n", tx_diff); // En millisecondes.
	clGetEventProfilingInfo(event, CL_PROFILING_COMMAND_START, sizeof(time_start), &time_start, NULL);
	clGetEventProfilingInfo(event, CL_PROFILING_COMMAND_END, sizeof(time_end), &time_end, NULL);
	total_time = time_end-time_start;
	printf("time (GPU point of view): %f \n", total_time / 1000000.0); // En millisecondes.

	/* Affichage des resultats. */
	printf("result: '%s'\n", data);

	/* Liberation des ressources. */
	clReleaseMemObject(output);
	clReleaseProgram(program);
	clReleaseKernel(kernel);
	clReleaseCommandQueue(commands);
	clReleaseContext(context);

	return 0;
}
