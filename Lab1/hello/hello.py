#!/usr/bin/env python
# -*- coding: utf-8 -*-

import numpy as np
import pyopencl as cl

import sys

# Lecture du fichier source.

filename = "hello.cl"
f = open(filename, 'r')
source = "".join(f.readlines())
f.close()

# Create de l'espace memoire qui va recevoir le resultat.

sz = 1024
string = np.random.rand(sz).astype(np.byte)

# Creation et configuration du device, de la queue.

ctx = cl.create_some_context()
queue = cl.CommandQueue(ctx)
mf = cl.mem_flags
string_gpu = cl.Buffer(ctx, mf.WRITE_ONLY, sz)

# Compilation.

prg = cl.Program(ctx, source).build()

# Execution et recuperation du resultat.

prg.hello(queue, string.shape, None, string_gpu)
cl.enqueue_copy(queue, string, string_gpu)

# Affichage.

print("".join([ chr(x) for x in string ]))
