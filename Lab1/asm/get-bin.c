#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <CL/opencl.h>

#define SRC_SZ_MAX (1024)
#define DATA_SZ_MAX (1024)

#define ASSERT_MSG(test, ...)          \
	if (! (test)) {                    \
		fprintf (stderr, __VA_ARGS__); \
		exit(1);                       \
	}

void read_file(char *filename, char *destination, int max_size) {
	FILE *f = fopen(filename, "r");
	ASSERT_MSG(f, "Err: read_file\n");
	fread(destination, 1, max_size, f);
	fclose(f);
}

int main(int argc, char** argv) {
	int err;
	char source_array[SRC_SZ_MAX];
	char *source = &source_array[0];
	cl_device_id device_id = NULL;
	cl_context context = NULL;
	cl_command_queue commands = NULL;
	cl_program program;

	ASSERT_MSG(argc == 2, "Usage: %s file\n", argv[0]);

	printf("Lors de l'utilisation d'un device nvidia, la sortie est le code assembleur lisible.\n");

	read_file(argv[1], source, SRC_SZ_MAX);

	err = clGetDeviceIDs(NULL, CL_DEVICE_TYPE_GPU, 1, &device_id, NULL);
	ASSERT_MSG(err == CL_SUCCESS, "Err: clGetDeviceIDs\n");

	context = clCreateContext(0, 1, &device_id, NULL, NULL, &err);
	ASSERT_MSG(context, "Err: clCreateContext\n");

	commands = clCreateCommandQueue(context, device_id, 0, &err);
	ASSERT_MSG(commands, "Err: clCreateCommandQueueWithProperties\n");

	program = clCreateProgramWithSource( context, 1, (const char **) &source, NULL, &err);
	ASSERT_MSG(program, "Err: clCreateProgramWithSource\n");

	err = clBuildProgram(program, 1, &device_id, NULL, NULL, NULL);
	if (err != CL_SUCCESS) {
		size_t len;
		char buffer[2048];
		clGetProgramBuildInfo(
				program, device_id, CL_PROGRAM_BUILD_LOG,
				sizeof(buffer), buffer, &len);
		fprintf(stderr, "Err: clBuildProgram:\n%s\n", buffer);
		exit(1);
	}

	/* Recuperation du code binaire du program. */
	size_t size;
	err = clGetProgramInfo(program, CL_PROGRAM_BINARY_SIZES, sizeof(size_t), &size, NULL);
	ASSERT_MSG(err == CL_SUCCESS, "Err: clGetProgramInfo\n");
	
	printf("prg binary size: %ld\n", size);
	
	unsigned char *binary = malloc(size);
	ASSERT_MSG(binary, "Err: malloc\n");
	
	err = clGetProgramInfo(program, CL_PROGRAM_BINARIES, size, &binary, NULL);
	ASSERT_MSG(err == CL_SUCCESS, "Err: clGetProgramInfo\n");
	
	FILE *f = fopen("bin", "wb");
	ASSERT_MSG(f, "Err: fopen\n");
	
	fwrite(binary, 1, size, f);
	fclose(f);
	
	free(binary);

	clReleaseProgram(program);
	clReleaseCommandQueue(commands);
	clReleaseContext(context);

	return 0;
}
